FROM public.ecr.aws/lambda/python:3.9

# Important for better signal handling
ENV DUMB_INIT_VERSION=1.2.5
RUN curl -L -o /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}/dumb-init_${DUMB_INIT_VERSION}_x86_64 && \
    chmod +x /usr/local/bin/dumb-init

RUN mkdir /app
WORKDIR /app
COPY requirements.txt  .
COPY santacloud/entrypoint.sh  .
RUN  pip3 install -r requirements.txt
COPY santacloud .

USER 65534
ENTRYPOINT ["dumb-init", "--single-child", "/app/entrypoint.sh"]