set -ue

ENVIRONMENT=$1
IMAGE=$2
COMMIT_HASH=$3
NAMESPACE="$ENVIRONMENT-santacloud"

echo 'Installing Santacloud...'
kubectl create namespace "$NAMESPACE" || true
helm upgrade -i --atomic --timeout=3m --cleanup-on-fail --history-max=10 \
  --namespace "$NAMESPACE" \
  --set image.repository="${IMAGE}:${COMMIT_HASH}" \
  --set serviceAccount.annotations."eks\.amazonaws\.com/role-arn"="arn:aws:iam::${AWS_ACCOUNT_ID}:role/santacloud-app" \
  -f deploy."$ENVIRONMENT".values.yaml \
  "$NAMESPACE" helm/santacloud