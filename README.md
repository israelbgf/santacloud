# santacloud

A project aiming to help Santa Claus to receive and process digital letters through a REST API (kids prefer REST to physical letters these days). Production grade application (or not quite) ready to be deployed at AWS.

## Features

- [x] Showcase of Clean Architecture concepts and Testing Principles
- [x] Infrastructure as a code from the ground up (90%).
- [x] No hanging credentials and more precise attribution with IRSA (IAM Roles for Service Accounts).
- [x] Cluster Autoscaler.
- [x] Gitlab Runner CI/CD inside the cluster.
- [x] AWS Loadbalancer Controller (replaces the deprecated builtin implementation).
- [x] docker-compose along with localstack for easier local development
- [x] HaProxy as ingress controller which has a flexible syntax to implement a WAF.
- [x] HTTPS through AWS ACM certificate.
- [x] Autoscaling with KEDA
- [x] Celery for Async Queues with Redis as Broker.
- [x] Prometheus and Metric Server for metric gathering + Grafana for better UI (Poor implementation without dashboards)

## Things to do

- [ ] Implement an actual monitoring dashboard. `kubectl top pod/node` and `Grafana Explore` is just a workaround.
- [ ] Being able to store Grafana dashboards at VCS. Grafana Operator could be useful, but maybe it's an overkill. 
- [ ] AWS PrivateLink for reducing NAT Gateway costs. Huge deal if using Fargate.
- [ ] ECR image cleaning rules.
- [ ] Alerting with AWS SNS.
- [ ] Prometheus Exporter for Redis Queues + KEDA Autoscaling for queue workers autoscaling.
- [ ] Move monitoring resources from terraform to a distinct directory. That big terraform file is starting to take too much processing time.
- [ ] Implement remaining backend tests.
- [ ] Use Haproxy to implement RateLimiting and more rules against crawlers.
- [ ] Review terraform naming conventions.
- [ ] Refine helm templates, the generated one is too verbose and isn't ready for k8s 1.22.
- [ ] Smoke tests with `helm test`
- [ ] Multiple environments (at least a Staging release specific for the `staging` branch)

## How to use it

To bootstrap all the application. You'll need a Gitlab Runner token and an ACM certificate:

```shell
cd terraform
terraform apply
aws eks --region us-east-1 update-kubeconfig --name santacloud
```

Unfortunately, setting DNS records are still manually done. So do it by yourself at your hosting provider.

## First Deploy

Redis should be installed manually:

```shell
./deploy.redis.sh production
```

After that, just make some pushes to branch `main` and let the magic happen!

## Local Playground

To test the application locally:

```shell
docker-compose build
docker-compose up
```

Be greeted:

```shell
curl http://localhost:8000
```

Send letters:

```shell
curl http://localhost:8000 \
  -XPOST -H 'content-type: application/json' \
  -d '{"name": "Bright Kid", "address": "Brazil", "content": "I want a PS5!"}'
```

See letters (Santa Admin functionality)

```shell
curl http://localhost:8000/letters -H "Authorization: Bearer s4nt4backd00r"
```

## Monitoring (Humble Edition™)

Accesing prometheus inside the cluster:

```shell
k port-forward -n monitoring prometheus-server-0 9090:9090
```

But you can have a better UI and even create some dashboards with Grafana. (auth: admin/admin):

```shell
kubectl port-forward -n monitoring svc/grafana 9090:3000
```

"Where's the famous Kubernetes Dashboard?", you may ask. Santa like CLI so let's stay with vanilla k8s tooling:

```shell
kubectl describe nodes
kubectl top pod --all-namespaces --use-protocol-buffers
kubectl top node --use-protocol-buffers
```
