module "santa_cloud_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "santacloud-application-storage"
  acl    = "private"

  versioning = {
    enabled = true
  }

}
