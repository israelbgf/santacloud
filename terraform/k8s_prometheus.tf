resource "helm_release" "prometheus" {
  depends_on = [
    module.eks
  ]

  name             = "prometheus"
  namespace        = "monitoring"
  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "prometheus"
  version          = "15.0.1"
  create_namespace = true

  values = [
    file("k8s_prometheus.values.yaml")
  ]
}