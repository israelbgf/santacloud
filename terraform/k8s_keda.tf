resource "helm_release" "keda" {
  depends_on = [
    module.eks
  ]

  name             = "keda"
  namespace        = "keda"
  repository       = "https://kedacore.github.io/charts"
  chart            = "keda"
  version          = "2.5.0"
  create_namespace = true
}