resource "helm_release" "haproxy-ingress-controller" {
  depends_on = [
    module.eks
  ]

  name             = "haproxy"
  namespace        = "haproxy-ingress"
  repository       = "https://haproxytech.github.io/helm-charts"
  chart            = "kubernetes-ingress"
  version          = "1.17.11"
  create_namespace = true

  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-ssl-cert"
    value = var.aws_acm_certificate
  }

  values = [
    file("k8s_haproxy_ingress_controller.values.yaml")
  ]
}