resource "helm_release" "metrics-server" {
  depends_on = [
    module.eks
  ]

  name             = "metrics-server"
  namespace        = "monitoring"
  repository       = "https://kubernetes-sigs.github.io/metrics-server/"
  chart            = "metrics-server"
  version          = "3.7.0"
  create_namespace = true
}