module "iam_assumable_role_santacloud_app" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 4.0"

  create_role                   = true
  role_name                     = "santacloud-app"
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.santacloud-app.arn]
  oidc_fully_qualified_subjects = [
    "system:serviceaccount:production-santacloud:production-santacloud",
  ]
}

resource "aws_iam_policy" "santacloud-app" {
  name_prefix = "santacloud-app"
  policy      = data.aws_iam_policy_document.santacloud-app.json
}

data "aws_iam_policy_document" "santacloud-app" {
  statement {
    sid    = "1"
    effect = "Allow"

    actions = [
      "s3:*", # TODO: Too permissive
    ]

    resources = [
      module.santa_cloud_bucket.s3_bucket_arn,
      "${module.santa_cloud_bucket.s3_bucket_arn}/*",
    ]
  }

}