resource "aws_ecr_repository" "santacloud" {
  name                 = "santacloud"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

# Caching repository used for building images with Kaniko
resource "aws_ecr_repository" "santacloud_cache" {
  name                 = "santacloud/cache"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository" "cicd_tools" {
  name                 = "cicd-tools"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

# Caching repository used for building images with Kaniko
resource "aws_ecr_repository" "cicd_tools_cache" {
  name                 = "cicd-tools/cache"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}
