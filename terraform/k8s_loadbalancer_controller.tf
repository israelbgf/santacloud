resource "helm_release" "loadbalancer-controller" {
  depends_on = [
    module.eks
  ]

  name             = "loadbalancer-controller"
  namespace        = "kube-system"
  repository       = "https://aws.github.io/eks-charts"
  chart            = "aws-load-balancer-controller"
  version          = "1.3.3"
  create_namespace = false

  set {
    name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = module.iam_assumable_role_loadbalancer_controller.iam_role_arn
  }

  set {
    name  = "serviceAccount.name"
    value = "aws-load-balancer-controller"
  }

  set {
    name  = "clusterName"
    value = module.eks.cluster_id
  }
}

module "iam_assumable_role_loadbalancer_controller" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 4.0"

  create_role                   = true
  role_name                     = "aws-load-balancer-controller"
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.loadbalancer-controller.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:aws-load-balancer-controller"]
}

resource "aws_iam_policy" "loadbalancer-controller" {
  name_prefix = "loadbalancer-controller"
  policy      = data.aws_iam_policy_document.loadbalancer-controller.json
}

data "aws_iam_policy_document" "loadbalancer-controller" {
  source_json = file("k8s_loadbalancer_controller.policy.json")

  statement {
    sid    = "1"
    effect = "Allow"

    actions = [
        "ec2:AuthorizeSecurityGroupIngress",
        "ec2:RevokeSecurityGroupIngress"
    ]

    resources = ["*"]
  }
}