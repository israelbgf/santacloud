output "app_service_account_role" {
  value       = module.iam_assumable_role_santacloud_app.iam_role_arn
  description = "Role ARN to setup IRSA with the app ServiceAccount."
}

output "app_s3_bucket" {
  value       = module.santa_cloud_bucket.s3_bucket_arn
  description = "ARN for the app bucket."
}