data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "santacloud"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true # Just to reduce costs.
  enable_dns_hostnames = true

  tags = {
    Terraform = "true"
  }

  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = 1 # Needed by AWS Loadbalancer Controller
  }

  public_subnet_tags = {
    "kubernetes.io/role/elb" = 1 # Needed by AWS Loadbalancer Controller
  }
}

module "eks" {
  source = "terraform-aws-modules/eks/aws"

  cluster_version = "1.21"
  cluster_name    = "santacloud"
  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.private_subnets
  enable_irsa     = true

  node_groups = {
    default = {
      instance_types   = ["t3.medium"]
      capacity_type    = "SPOT"
      desired_capacity = 1
      max_capacity     = 3
      min_capacity     = 1

      update_config   = {
        max_unavailable = 1
      }
      additional_tags = {
        "k8s.io/cluster-autoscaler/enabled"    = "TRUE" # Needed by Cluster Autoscaler
        "k8s.io/cluster-autoscaler/${module.eks.cluster_id}" = "owned" # Needed by Cluster Autoscaler
      }
    }
  }

}