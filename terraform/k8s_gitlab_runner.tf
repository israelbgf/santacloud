locals {
  gitlab_runner_release   = "gitlab-runner"
  gitlab_runner_namespace = "gitlab-runner"
}


resource "helm_release" "gitlab-runner" {
  depends_on = [
    module.eks
  ]

  name             = local.gitlab_runner_release
  namespace        = local.gitlab_runner_namespace
  repository       = "https://charts.gitlab.io/"
  chart            = "gitlab-runner"
  version          = "0.35.0"
  create_namespace = true

  set {
    name  = "runnerRegistrationToken"
    value = var.gitlab_runner_registration_token
  }

  set {
    name  = "rbac.serviceAccountAnnotations.eks\\.amazonaws\\.com/role-arn"
    value = module.iam_assumable_role_gitlab_runner.iam_role_arn
  }

  values = [
    file("k8s_gitlab_runner.values.yaml")
  ]
}


module "iam_assumable_role_gitlab_runner" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 4.0"

  create_role                   = true
  role_name                     = "gitlab-runner"
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.gitlab_runner.arn]
  oidc_fully_qualified_subjects = [
    "system:serviceaccount:${local.gitlab_runner_namespace}:gitlab-runner-${local.gitlab_runner_release}"
  ]
}

resource "aws_iam_policy" "gitlab_runner" {
  name_prefix = "gitlab-runner"
  policy      = data.aws_iam_policy_document.gitlab_runner.json
}

data "aws_iam_policy_document" "gitlab_runner" {
  statement {
    sid    = "1"
    effect = "Allow"

    actions = [
      "ecr:*", # TODO: Too permissive.
    ]

    resources = ["*"]
  }

}

# TODO: Too permissive. Best solution is to change this approach and use a "GitOps style".
resource "kubernetes_cluster_role_binding" "gitlab_runner_admin" {
  depends_on = [
    helm_release.gitlab-runner
  ]

  metadata {
    name = "gitlab-runner-admin"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-runner-${local.gitlab_runner_release}"
    namespace = local.gitlab_runner_namespace
  }
}