variable "aws_acm_certificate" {
  type        = string
  description = "Create your certificate with ACM at https://console.aws.amazon.com/acm/home?region=us-east-1#/certificates/list"
}

variable "gitlab_runner_registration_token" {
  type        = string
  description = "Get your Gitlab Runner registration token at: https://gitlab.com/israelbgf/santacloud/-/settings/ci_cd"
}
