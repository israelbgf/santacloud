resource "helm_release" "grafana" {
  depends_on = [
    module.eks
  ]

  name             = "grafana"
  namespace        = "monitoring"
  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "grafana"
  version          = "7.2.11"
  create_namespace = true

  set {
    name  = "admin.user"
    value = "admin"
  }

  set {
    name  = "admin.password"
    value = "admin"
  }
}