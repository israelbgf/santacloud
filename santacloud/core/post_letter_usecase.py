def post_letter(data, processing_gateway):
    name = data.get('name')
    address = data.get('address')
    content = data.get('content')

    if all([name, address, content]):
        processing_gateway({
            'name': name,
            'address': address,
            'content': content,
        })

        return {'message': 'Hohoho... I hope that you was a good kid!'}
    else:
        return {'error': 'Before getting a gift, you should learn how to send a letter kiddo!'}
