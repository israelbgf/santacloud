import time

import settings


def process_letter(letter, storage_gateway):
    is_worthy = _elfic_algorithm_for_finding_out_worthy_kids(letter)
    if is_worthy:
        storage_gateway('worthy-kids-that-needs-gifts', letter)
    else:
        storage_gateway('coal-dispatching-list', letter)


def _elfic_algorithm_for_finding_out_worthy_kids(letter):
    elf_parallelism = 10
    for i in range(elf_parallelism):
        print(f'Deploying christmas elf worker #{i} for IA computation')
        time.sleep(settings.elf_processing_time)

    return len(letter['content']) >= 100  # The sad story about what means to be worthy to Santa.
