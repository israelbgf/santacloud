from unittest.mock import Mock, patch

from core.worthy_processing import process_letter


def test_worthy_when_letter_have_more_than_100_characters():
    storage_gateway = Mock()
    worthy_letter = {'content': 100 * 'a'}

    with patch('time.sleep', return_value=None) as _:
        process_letter(worthy_letter, storage_gateway)

    storage_gateway.assert_called_with('worthy-kids-that-needs-gifts', worthy_letter)


def test_unworthy_when_letter_have_less_than_100_characters():
    storage_gateway = Mock()
    worthy_letter = {'content': 99 * 'a'}

    with patch('time.sleep', return_value=None) as _:
        process_letter(worthy_letter, storage_gateway)

    storage_gateway.assert_called_with('coal-dispatching-list', worthy_letter)
