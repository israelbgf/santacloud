from http import HTTPStatus

from flask import Flask, request
from flask import jsonify
from flask_httpauth import HTTPTokenAuth

from core import post_letter_usecase
import os

from gateways import gateway_celery, gateway_s3

app = Flask(__name__)
auth = HTTPTokenAuth(scheme='Bearer')

PROCESSING_GATEWAY = gateway_celery.proccess_letter.delay


@auth.verify_token
def verify_token(token):
    if os.environ.get('AUTH_TOKEN', 's4nt4backd00r') == token:
        return True


@app.route("/", methods=['GET'])
def index():
    return "Ho-ho-ho!", HTTPStatus.OK


@app.route("/", methods=['POST'])
def post_letter():
    response = post_letter_usecase.post_letter(request.json or {}, processing_gateway=PROCESSING_GATEWAY)

    success = 'message' in response
    return jsonify(response), HTTPStatus.OK if success else HTTPStatus.UNPROCESSABLE_ENTITY


@app.route("/letters", methods=['GET'])
@auth.login_required
def get_letters():
    return jsonify(list(gateway_s3.list()))


@app.route("/healthcheck", methods=['GET'])
def healthcheck():
    return "OK", HTTPStatus.OK


if __name__ == '__main__':
    app.run()
