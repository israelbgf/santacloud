import pytest

import settings
from gateways import gateway_celery


@pytest.fixture
def client():
    from api.app import app
    app.config.update({'TESTING': True})

    from gateways.gateway_celery import app as celery
    celery.conf.task_always_eager = True
    celery.conf.task_eager_propagates = True

    with app.test_client() as client:
        yield client


def test_error_when_empty_request(client):
    response = client.post('/')
    assert "should learn" in str(response.data)
    assert "422" in response.status


def test_success_when_valid_request(client):
    gateway_celery.STORAGE_GATEWAY = lambda x, y: True  # Test Double: Stub
    settings.elf_processing_time = 0

    response = client.post('/', json={
        'name': 'Jhon Doe',
        'address': 'Unknown Place',
        'content': 'Im a good kid sir!'
    })

    assert "Hohoho" in str(response.data)
    assert "200" in response.status
