#!/bin/bash
set -ue

api() {
  exec gunicorn api.app:app --bind=0.0.0.0:8000
}

worker() {
  celery --app gateways.gateway_celery worker -Qsantacloud_default -l info
}

if [[ $1 == "api" ]]; then
    api
elif [[ $1 == "queue" ]]; then
    worker
else
  echo "Invalid argument. Must be 'api' or 'queue'"
  exit 1
fi