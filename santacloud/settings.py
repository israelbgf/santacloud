import os

aws_endpoint_url = os.environ.get('AWS_ENDPOINT_URL', 'http://localstack:4566') or None
aws_bucket = os.environ.get('AWS_BUCKET', 'santacloud')
redis_address = os.environ.get('REDIS_ADDR', 'redis://redis:6379/0')
elf_processing_time = int(os.environ.get('ELF_PROCESSING_TIME', 1))
