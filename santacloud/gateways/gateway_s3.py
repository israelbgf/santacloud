import uuid

import boto3

import settings
import json


def save(location, content):
    client = boto3.client('s3', endpoint_url=settings.aws_endpoint_url)
    response = client.put_object(
        Body=json.dumps(content),
        Bucket=settings.aws_bucket,
        Key=f"{location}/{uuid.uuid4()}-{content['name']}",
        ServerSideEncryption='AES256',
        StorageClass='STANDARD_IA'
    )
    print(response)


def list():
    s3 = boto3.resource('s3', endpoint_url=settings.aws_endpoint_url)
    bucket = s3.Bucket(settings.aws_bucket)
    for obj in bucket.objects.all():
        yield obj.key
