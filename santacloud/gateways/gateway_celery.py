from celery import Celery

import settings
from core.worthy_processing import process_letter
from gateways import gateway_s3

app = Celery(
    broker=settings.redis_address,
    backend=settings.redis_address,
    task_default_queue="santacloud_default",
)

STORAGE_GATEWAY = gateway_s3.save


@app.task
def proccess_letter(letter):
    process_letter(letter, storage_gateway=STORAGE_GATEWAY)
