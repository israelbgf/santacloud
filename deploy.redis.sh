set -ue

ENVIRONMENT=$1
NAMESPACE="$ENVIRONMENT-santacloud"

echo 'Installing Redis dependency...'
kubectl create namespace "$NAMESPACE" || true
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install redis bitnami/redis --version "15.6.4" \
  --namespace "$NAMESPACE" \
  --set auth.enabled=false \
  --set architecture=standalone